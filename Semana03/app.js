// npm install
// Va obtener el paquete y lo asiganra a express
// import library
// #include library.h
const express = require("express");
// Inicializamos express
const app = express();
// Puerto sobre el cual funcionara
const port = 3000;
// Uso de middleware para archivos estaticos
app.use(express.static('public'));


// metodo GET sin URL
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// metodo GET sin URL
app.get("/greeting", (req, res) => {
  res.send("Hello!");
});

// metodo GET sin URL
app.post("/", (req, res) => {
  res.send("Hello POST!");
});

// metodo GET sin URL
app.post("/greeting", (req, res) => {
  res.send("Greetings POST!");
});

// Escuchar nuestra aplicacion
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
