// console.log("test");
var req = new XMLHttpRequest();
req.open("GET", "https://api.openbrewerydb.org/breweries", true);
req.onreadystatechange = function () {
  if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
    // console.log(req.responseText);
    console.log(JSON.parse(req.responseText));
  }
};
req.send();

fetch("https://api.openbrewerydb.org/breweries")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    return data;
  })
  .then((data) => console.log(data));

// Ejemplo implementando el metodo POST:
async function postData(url = "", data = {}) {
  // Opciones por defecto estan marcadas con un *
  const response = await fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

postData("https://example.com/answer", { answer: 42 }).then((data) => {
  console.log(data); // JSON data parsed by `data.json()` call
});
