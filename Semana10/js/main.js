function insertItem() {
  console.log("On Click");
  let formInsertItem = document.querySelector("#form-insert-item");
  let form = document.forms.namedItem("insertItem");
  console.log(formInsertItem.name.value);
  //   let file = formInsertItem.elements.filename.files[0];
  //   let formData = new FormData(form);
  let insert = {
    name: formInsertItem.name.value,
    file: formInsertItem.image,
  };
  let request = new XMLHttpRequest();
  request.open("POST", "http://localhost:3000/v1/item");
  // Set Request Header debe ser agregado despues del open
  //   request.setRequestHeader("Content-Type", "multipart/form-data");
  request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  request.send(JSON.stringify(insert));
}

function getItems() {
  let request = new XMLHttpRequest();
  let selectItems = document.querySelector("#items");
  request.open("GET", "http://localhost:3000/v1/item");
  request.onreadystatechange = (data) => {
    if (request.readyState == 4 && request.status == 200) {
      let info = JSON.parse(data.target.responseText);
      console.log(info[0]);
      selectItems.innerHTML = "";
      for (const inf of info[0]) {
        selectItems.innerHTML += `<option value="${inf.iditems}">${inf.itemsName}</option>`;
      }
    }
  };
  request.send();
}

function updateItem() {
  console.log("On Click");
  let formInsertItem = document.querySelector("#form-insert-item");

  let update = {
    id: formInsertItem.name.value,
    name: formInsertItem.name.value,
    file: formInsertItem.image,
  };
  let request = new XMLHttpRequest();
  request.open("PUT", "http://localhost:3000/v1/item");
  request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  request.send(JSON.stringify(update));
}

window.onload = function () {
  console.log("Load");
  // Obtenemos el element por medio de un querySelector
  let buttonInsertItem = document.querySelector("#button-insert-item");
  // Agregamos el evento al click
  buttonInsertItem.addEventListener("click", insertItem);

  let showItemList = document.querySelector("#showItemsLists");

  showItemList.addEventListener("click", getItems);

  // Obtenemos el element por medio de un querySelector
  let buttonUpdateItem = document.querySelector("#button-update-item");
  // Agregamos el evento al click
  buttonUpdateItem.addEventListener("click", updateItem);
};
