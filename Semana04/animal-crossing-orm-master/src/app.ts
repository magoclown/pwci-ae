import categoryRoutes from "./routes/category.routes";
import fashionRoutes from "./routes/fashion.routes";
import itemRoutes from "./routes/item.routes";
import sellerRoutes from "./routes/seller.routes";
import subcategoryRoutes from "./routes/subcategory.routes";
import userRoutes from "./routes/user.routes";
import catalogRoutes from "./routes/catalog.routes";

const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const typeorm = require("typeorm");
const config = require("./config");

typeorm.createConnections([
    config.dbConfig
]);

const controllers = [
  categoryRoutes,
  fashionRoutes,
  itemRoutes,
  sellerRoutes,
  subcategoryRoutes,
  userRoutes,
  catalogRoutes
];

const app = express();
app.use(cors());
app.use(express.json());
app.use(morgan("dev"));

app.use(controllers);

app.listen(config.defaultPort);
console.log("Server on port", config.defaultPort);
