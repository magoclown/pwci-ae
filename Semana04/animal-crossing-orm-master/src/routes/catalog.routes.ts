import { Router } from "express";

import {
  getCatalogs,
  createCatalog,
  updateCatalog,
  deleteCatalog,
  getCatalogByUserID,
  addItemToCatalog,
  removeItemToCatalog,
  getCatalog
} from "../controllers/catalog.controller";

const router = Router();

router.get("/catalogs", getCatalogs);
router.get("/catalogs/:id", getCatalog);
router.get("/catalogs/user/:id", getCatalogByUserID);
router.post("/catalogs", createCatalog);
router.post("/catalogs/:id/item/", addItemToCatalog);
// router.post("/catalogs/user", getCatalogByUserID);
router.put("/catalogs/:id", updateCatalog);
router.delete("/catalogs/:id/item", removeItemToCatalog);
router.delete("/catalogs/:id", deleteCatalog);

export default router;
