import { Router } from "express";

import {
  getSubcategories,
  getSubcategory,
  createSubcategory,
  updateSubcategory,
  getSubcategoriesWithCategory,
  getSubcategoriesWithItems,
  getSubcategoryWithItems,
} from "../controllers/subcategory.controller";

const router = Router();

router.get("/subcategories", getSubcategories);
router.get("/subcategories/wc", getSubcategoriesWithCategory);
router.get("/subcategories/wi", getSubcategoriesWithItems);
router.get("/subcategories/:id", getSubcategory);
router.get("/subcategories/wi/:id", getSubcategoryWithItems);
router.post("/subcategories", createSubcategory);
router.put("/subcategories/:id", updateSubcategory);
// router.delete('subcategories/:id', deleteSubcategory);

export default router;
