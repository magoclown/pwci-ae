import { getRepository } from "typeorm";
import { Response, Request } from "express";

import { Item } from "../entities/Item";

export const getItems = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).find({ order: { name: "ASC" } });
  return res.json(results);
};

export const getItemsWithAll = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).find({
    relations: ["subcategory", "seller", "fashion"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getItemsWithAllBySubcategoryId = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).find({
    relations: ["subcategory", "seller", "fashion"],
    where: { subcategory: req.params.id },
  });
  return res.json(results);
};

export const getItemsWithAllByCategoryId = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item)
    .createQueryBuilder("item")
    .leftJoinAndSelect("item.subcategory", "subcategory")
    .leftJoinAndSelect("item.seller", "seller")
    .leftJoinAndSelect("item.fashion", "fashion")
    .leftJoinAndSelect("subcategory.category", "category")
    .where("category.id = :id", { id: req.params.id })
    .orderBy("item.name")
    .getMany();
  return res.json(results);
};

export const getItemsWithAllBySubcategoryIdAndName = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item)
    .createQueryBuilder("item")
    .leftJoinAndSelect("item.subcategory", "subcategory")
    .leftJoinAndSelect("item.seller", "seller")
    .leftJoinAndSelect("item.fashion", "fashion")
    .leftJoinAndSelect("subcategory.category", "category")
    .where("subcategory.id = :id", { id: req.params.id })
    .andWhere("item.name LIKE :name", { name: `%${req.params.name}%` })
    .orderBy("item.name")
    .getMany();
  return res.json(results);
};

export const getItemWithAll = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).findOne(req.params.id, {
    relations: ["category", "seller", "fashion"],
    order: { name: "ASC" },
  });
  return res.json(results);
};

export const getItem = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).findOne(req.params.id);
  return res.json(results);
};

export const createItem = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const item = await getRepository(Item).create(req.body);
  const results = await getRepository(Item).save(item);
  return res.json(results);
};

export const updateItem = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const item = await getRepository(Item).findOne(req.params.id);
  if (item) {
    getRepository(Item).merge(item, req.body);
    const results = await getRepository(Item).save(item);
    return res.json(results);
  }
  return res.json({ msg: "Not found" });
};

export const deleteItem = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const results = await getRepository(Item).delete(req.params.id);
  return res.json(results);
};
