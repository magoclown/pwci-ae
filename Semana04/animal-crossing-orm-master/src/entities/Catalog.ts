import { PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne, Entity } from "typeorm";
import { Item } from "./Item";
import { User } from "./User";

@Entity({name: 'catalog'})
export class Catalog {
    @PrimaryGeneratedColumn({name: 'catalog_id'})
    id: number;
    @Column({name: 'catalog_name'})
    name: string;
    @ManyToOne(type => User, user => user.catalogs)
    user: User;    
    @ManyToMany(type => Item)
    @JoinTable()
    items: Item[];
}