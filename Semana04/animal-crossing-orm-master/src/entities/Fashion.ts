import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

import { Item } from "./Item";

@Entity({name: 'fashion'})
export class Fashion {
    @PrimaryGeneratedColumn({name: 'fashion_id'})
    id: number;
    @Column({name: 'fashion_name'})
    name: string;
    @OneToMany(type => Item, items => items.fashion)
    items: Item[];
}