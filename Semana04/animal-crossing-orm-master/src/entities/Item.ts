import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinTable, ManyToMany } from "typeorm";

import { Seller } from "./Seller";
import { Fashion } from "./Fashion";
import { Subcategory } from "./Subcategory";

@Entity({name: 'item'})
export class Item {
    @PrimaryGeneratedColumn({name: 'item_id'})
    id: number;
    @Column({name: 'item_is_diy'})
    isDIY: boolean;
    @Column({name: 'item_is_customizable'})
    isCustomizable: boolean;
    @Column({name: 'item_name'})
    name: string;
    @Column({name: 'item_image'})
    image: string;
    @Column({name: 'item_purchase_price'})
    purchasePrice: number;
    @Column({name: 'item_sells_price'})
    sellsPrice: number;
    @ManyToOne(type => Subcategory, subcategory => subcategory.items)
    subcategory: Subcategory;
    @ManyToOne(type => Seller, seller => seller.items)
    seller: Seller;
    @ManyToOne(type => Fashion, fashion => fashion.items)
    fashion: Fashion;
}