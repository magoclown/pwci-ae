import {Request, Response} from "express";
import {createConnection} from "typeorm";
import { Item } from "./entity/Item";
import { List } from "./entity/List";
import {User} from "./entity/User";

const express = require("express");

// create typeorm connection
createConnection().then(connection => {
    const userRepository = connection.getRepository(User);
    const listRepository = connection.getRepository(List);
    const itemRepository = connection.getRepository(Item);

    // create and setup express app
    const app = express();
    app.use(express.json());

    // register routes

    app.get("/users", async function(req: Request, res: Response) {
        const users = await userRepository.find();
        res.json(users);
    });

    app.get("/users/:id", async function(req: Request, res: Response) {
        const results = await userRepository.findOne(req.params.id);
        return res.send(results);
    });

    app.post("/users", async function(req: Request, res: Response) {
        const user = await userRepository.create(req.body);
        const results = await userRepository.save(user);
        return res.send(results);
    });

    app.put("/users/:id", async function(req: Request, res: Response) {
        const user = await userRepository.findOne(req.params.id);
        userRepository.merge(user, req.body);
        const results = await userRepository.save(user);
        return res.send(results);
    });

    app.delete("/users/:id", async function(req: Request, res: Response) {
        const results = await userRepository.delete(req.params.id);
        return res.send(results);
    });

    // lists

    app.get("/lists", async function(req: Request, res: Response) {
        const users = await listRepository.find({ relations: ["items"] });
        res.json(users);
    });

    app.get("/lists/:id", async function(req: Request, res: Response) {
        const results = await listRepository.findOne(req.params.id, { relations: ["items"] });
        return res.send(results);
    });

    app.post("/lists", async function(req: Request, res: Response) {
        const user = await listRepository.create(req.body);
        const results = await listRepository.save(user);
        return res.send(results);
    });

    // items

    app.get("/items", async function(req: Request, res: Response) {
        const users = await itemRepository.find({ relations: ["lists"] });
        res.json(users);
    });

    app.get("/items/:id", async function(req: Request, res: Response) {
        const results = await itemRepository.findOne(req.params.id, { relations: ["lists"] });
        return res.send(results);
    });

    app.post("/items", async function(req: Request, res: Response) {
        const user = await itemRepository.create(req.body);
        const results = await itemRepository.save(user);
        return res.send(results);
    });

    // start express server
    app.listen(3000);
});