import {Entity, Column, PrimaryGeneratedColumn, ManyToMany} from "typeorm";
import { Item } from "./Item";

@Entity()
export class List {
    
    // {
    //     "name": "Navidad",
    //     "description": "Lista para navidad"
    // }
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @ManyToMany(() => Item,  items=> items.lists)
    items: Item[];

}