import {Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable} from "typeorm";
import { List } from "./List";

@Entity()
export class Item {

    // {
    //     "name": "Corona de Navidad",
    //     "description": "Adorno para montar en puertas o paredes",
    //     "lists": [
    //         {"id": 2}
    //     ]
    // }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @ManyToMany(() => List,  lists=> lists.items)
    @JoinTable()
    lists: List[];

}