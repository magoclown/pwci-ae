const express = require("express");
const app = express();
const port = 3000;
var mysql = require("mysql");
var cors = require("cors");
app.use(express.json());
app.use(cors());

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "api",
});
connection.connect();

app.get("/", (req, res) => {
  res.send("Hola");
});

app.get("/v1/user", (req, res) => {
  connection.query(`CALL get_all_users();`, function (err, rows, fields) {
    if (err) throw err;
    res.send(rows);
  });
});

app.post("/v1/user", (req, res) => {
  console.log(req.body);
  connection.query(
    `CALL insert_user('${req.body.nickname}', '${req.body.password}', '${req.body.email}');`,
    function (err, rows, fields) {
      if (err) throw err;
      res.send(rows);
    }
  );
});

app.post("/v1/user/login", (req, res) => {
  console.log(req.body);
  connection.query(
    `CALL login_user('${req.body.nickname}', '${req.body.password}');`,
    function (err, rows, fields) {
      if (err) throw err;
      res.send(rows[0]);
    }
  );
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
