import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { SinginUserComponent } from './singin-user/singin-user.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginUserComponent,
    SinginUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
