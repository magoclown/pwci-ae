import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css'],
})
export class LoginUserComponent implements OnInit {
  public nickname: string;
  public password: string;
  public alert: string;

  constructor(private userService: UsersService) {}

  ngOnInit(): void {}

  public onSubmit(e): void {
    e.preventDefault();
    this.userService.login(this.nickname, this.password).subscribe((data) => {
      console.log(data);
      if (data[0].Exist === 1) {
        this.alert = 'Ingreso exitoso';
      } else {
        this.alert = 'Datos Incorrectos';
      }
    });
  }

  public onChangePassword(e): void {
    this.password = e.target.value;
  }

  public onChangeNickname(e): void {
    this.nickname = e.target.value;
  }
}
