import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginModel } from '../models/loginModel';
import { SinginModel } from '../models/SinginModel';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  public login(nickname: string, password: string): Observable<{}>  {
    console.log('Call login');
    return this.http.post<LoginModel>('http://localhost:3000/v1/user/login', new LoginModel(nickname, password));
  }
  public signin(email: string, nickname: string, password: string): Observable<{}>  {
    console.log('Call SignIn');
    return this.http.post<SinginModel>('http://localhost:3000/v1/user', new SinginModel(email, nickname, password));
  }
}
