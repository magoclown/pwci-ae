import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-singin-user',
  templateUrl: './singin-user.component.html',
  styleUrls: ['./singin-user.component.css']
})
export class SinginUserComponent implements OnInit {

  public nickname: string;
  public password: string;
  public email: string;
  public alert: string;

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
  }

  public onSubmit(e): void {
    e.preventDefault();
    this.userService.signin(this.email, this.nickname, this.password).subscribe((data) => {
      console.log(data);
      if (data[0].Exist === 1) {
        this.alert = 'Ingreso exitoso';
      } else {
        this.alert = 'Datos Incorrectos';
      }
    });
  }

  public onChangePassword(e): void {
    this.password = e.target.value;
  }

  public onChangeNickname(e): void {
    this.nickname = e.target.value;
  }

  public onChangeEmail(e): void {
    this.email = e.target.value;
  }

}
