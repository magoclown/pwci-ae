var divFormat = (username, password) => {
  return `<div class="user">
    <h2>User:${username}</h2>
    <p>Password:${password}</p>
  </div>`;
};

function sendUser(e) {
  let userForm = document.querySelector("form#user");
  e.preventDefault();
  console.log(userForm);
  console.log(e);
  var req = new XMLHttpRequest();
  req.open("POST", "http://localhost:3000/user", true);
  req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  req.send(
    JSON.stringify({
      user: userForm.user.value,
      password: userForm.password.value,
    })
  );
}

window.onload = function () {
  let userForm = document.forms["user"];
  userForm.addEventListener("submit", sendUser);
  var req = new XMLHttpRequest();
  req.open("GET", "http://localhost:3000/user", true);

  req.onreadystatechange = function (data) {
    if (req.readyState == 4 && req.status == 200) {
      var users = document.querySelector("div#users");
      var fecthData = data.target.response;
      var parse = JSON.parse(fecthData);
      console.log(fecthData);
      //   users.append(fecthData);
      //   console.log(parse);
      //   users.append(parse);
      parse.forEach((k, v) => {
        console.log(`K: ${k.usersName}`);
        console.log(`V: ${v}`);
        users.innerHTML += divFormat(k.usersName, k.userPassword);
      });
    }
  };
  req.send();
};
