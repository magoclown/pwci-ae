const express = require("express");
const app = express();
const port = 3000;
var mysql = require("mysql");
var cors = require("cors");
app.use(express.json());
app.use(cors());

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "api",
});
connection.connect();
// connection.connect();

// connection.query("SELECT 1 + 1 AS solution", function (err, rows, fields) {
//   if (err) throw err;
//   console.log("The solution is: ", rows[0].solution);
// });

// connection.end();

app.get("/", (req, res) => {
  connection.query("SELECT 1 + 1 AS solution", function (err, rows, fields) {
    if (err) throw err;
    console.log("The solution is: ", rows[0].solution);
  });
});

app.get("/user", (req, res) => {
  connection.query(
    `SELECT idusers,
    usersName,
    userPassword
    FROM users;`,
    function (err, rows, fields) {
      if (err) throw err;
      res.send(rows);
    }
  );
});

app.get("/user/:id", (req, res) => {
  connection.query(
    `SELECT idusers,
        usersName,
        userPassword
        FROM users
        WHERE idusers = ${req.params.id}`,
    function (err, rows, fields) {
      if (err) throw err;
      res.send(rows);
    }
  );
});

app.post("/user", (req, res) => {
  console.log(req.body);
  //   res.send(req.body);
  //   res.send(`Creacion de usuario`);
  //   console.log(req);
  //   console.log(req.body);
  connection.query(
    `INSERT INTO users(usersName,userPassword) VALUES('${req.body.user}','${req.body.password}');`,
    function (err, rows, fields) {
      if (err) throw err;
      res.send(rows);
    }
  );
});

app.put("/user/:id", (req, res) => {
  res.send(`Modificacion de usuario ${req.params.id}`);
});

app.delete("/user/:id", (req, res) => {
  res.send(`Eliminacion de usuario ${req.params.id}`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
