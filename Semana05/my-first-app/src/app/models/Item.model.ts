import { Title } from '@angular/platform-browser';

export class Item {
  title: string;
  image: string;
  description: string;

  constructor(title: string, image: string, description: string) {
    this.title = title;
    this.image = image;
    this.description = description;
  }
}
