import { Component, OnInit } from '@angular/core';
import { Item } from './models/Item.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Mi Primer Aplicacion';
  items: Item[] = [];

  ngOnInit(): void {
    this.items.push(new Item('Primero', 'https://cdnb.artstation.com/p/assets/images/images/028/539/971/4k/ina-ortega-inaortega-michi-3.jpg?1594760105', 'El gran y todo poderoso primero'));
    this.items.push(new Item('El Mas', 'https://cdnb.artstation.com/p/assets/images/images/026/399/463/large/francis-xavier-martins-redsm.jpg?1588683524', 'Sabroso de todo el norte'));
    this.items.push(new Item('Gran Banco', 'https://cdna.artstation.com/p/assets/images/images/028/069/748/4k/kevin-beckers-rex-2-00001.jpg?1593414051', 'Mas grande que los demas'));
  }
}
