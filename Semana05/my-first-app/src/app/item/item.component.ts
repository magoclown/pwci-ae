import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../models/Item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input()
  model: Item;

  constructor() { }

  ngOnInit(): void {
  }

}
